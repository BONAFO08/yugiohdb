const newCard = require("./create");
const db = require("../conexion");
const modCard = require("./modify");


const createAcard =(req,res)=>{
    newCard.createCard(req.body)
        .then(resp => res.status(200).json(`La carta ${resp.name} fue creada con exito`))
        .catch(err => (err != "La carta ya existe") ? (res.status(400).send("Error al crear la carta"))
            : (res.status(403).send(err)));
}


const addDescritionAcard =(req,res)=>{
    newCard.createDescription(req.params.id,req.body)
        .then(resp => res.status(200).json(`La descripcion de la carta ${resp.name} fue agregada on exito`))
        .catch(err => (err != "La carta no existe") ? (res.status(400).send("Error al crear descripcion"))
            : (res.status(403).send(err)));
}

const showAllDB =(req,res)=>{
    db.modelCard.find()
    .then(resolve => res.status(200).send(resolve))
    .catch(err => res.status(404).send(err));
}

const showACard =(req,res)=>{
    db.modelCard.findOne({YDB : req.params.id})
    .then(resolve =>{
        (resolve.length === 0) 
        ?("")
        :(res.status(200).json(resolve))
    }
    )
    .catch(err => res.status(404).send("La carta no existe"));
}

const deleteCard = async (req,res)=>{
    const aux = await db.modelCard.deleteOne({YDB : req.params.id});
    res.status(200).send("Carta eliminada exitosamente");
}

const deleteACard =(req,res)=>{
    db.modelCard.findOne({YDB : req.params.id})
    .then(resolve =>{
      
        (resolve.length === 0) 
        ?("")
        :(deleteCard(req,res))
    }
    )
    .catch(err => res.status(404).send("La carta no existe"));
}

const modifyACard =(req,res)=>{
    db.modelCard.findOne({YDB : req.params.id})
    .then(resolve =>{
        (resolve.length === 0) 
        ?("")
        :(modCard.modifyACard(req,res))
    }
    )
    .catch(err => res.status(404).send("La carta no existe"));
}

module.exports ={
    createAcard,
    addDescritionAcard,
    showAllDB,
    showACard,
    deleteACard,
    modifyACard
};