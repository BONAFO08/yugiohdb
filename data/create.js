const db = require("../conexion");
const search = require("./search");




const newCard = async (data) => {
    const tempCard = await new db.modelCard({
        name: data.name,
        nameJP: data.nameJP,
        starts: data.starts,
        atk: data.atk,
        def: data.def,
        YDB: data.ydb,
        type: data.type,
        tCard: data.tCard,
        attribute: data.attribute,
        urlImg: data.urlImg
    }
    );
    const result = await tempCard.save();
    return result;
};

const createCard = async (data) =>{
    let result;
    let validator =  await search.findOne(data.ydb);
    if(validator == false){
        result = await newCard(data);
        return result;
    }else{
        throw "La carta ya existe"
    }
}

const newDescription = async (card,desc)=>{
    card.description.push(desc)
    let result = await card.save();
    return result;
}

const createDescription = async (id,desc)=>{
    let result;
    let validator =  await search.findOne(id);
    if(validator === false){
        throw "La carta no existe"
    }else{
    result = await newDescription(validator[0],desc.description);
    return result;
    }
}


module.exports = {
    newCard,
    createCard,
    createDescription
};