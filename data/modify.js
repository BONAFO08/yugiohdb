const db = require("../conexion");
const search = require("./search");


const modifyACard = async (data, res) => {
    let validator = await search.findOne(data.params.id);
    const aux = await db.modelCard.updateMany({ YDB: data.params.id },
        {
            $set: {
                name: (data.body.name == undefined) ? (validator.name) : (data.body.name),
                nameJP: (data.body.nameJP == undefined) ? (validator.nameJP) : (data.body.nameJP),
                starts: (data.body.starts == undefined) ? (validator.starts) : (data.body.starts),
                atk: (data.body.atk == undefined) ? (validator.atk) : (data.body.atk),
                def: (data.body.def == undefined) ? (validator.def) : (data.body.def),
                type: (data.body.type == undefined) ? (validator.type) : (data.body.type),
                tCard: (data.body.tCard == undefined) ? (validator.tCard) : (data.body.tCard),
                attribute: (data.body.attribute == undefined) ? (validator.attribute) : (data.body.attribute),
                urlImg: (data.body.urlImg == undefined) ? (validator.urlImg) : (data.body.urlImg)
            }
        }
    );
    res.status(200).send("Carta modificada exitosamente");
}

module.exports = {
    modifyACard
};
