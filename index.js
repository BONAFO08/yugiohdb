const mongoose = require("./conexion");
const express = require("express");
const app = express();
const port = 3000;
const swaggerUI = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");

const swaggerOptions = {
   swaggerDefinition: {
       info: {
           title: 'Base de datos Yugioh',
           version: '1.0.0'
       }
   },
   apis: ['./routes.js'],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use('/api-docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs));

app.use(require("./routes"));

app.listen(port, ()=>{
   console.log(`SERVER ON PORT ${port}`); 
});