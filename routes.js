const express = require("express");
const router = express.Router();
const db = require("./conexion");
const middle = require("./data/middle");



//name, nameJP, starts, atk, def, YDB, type, tCard, attribute, urlImg

/**
 * @swagger
 * /:
 *  get:
 *    tags: 
 *      [Cards]
 *    summary: Mostrar Base de Datos
 *    description: Muestra toda la Base de Datos
 *    responses:
 *            200:
 *                description: Pedido exitoso
 *            404:
 *                description: Usuario o producto no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */


router.get("/", (req, res) => {
    middle.showAllDB(req, res);

})

/**
 * @swagger
 * /card/{id}:
 *  get:
 *    tags: 
 *      [Cards]
 *    summary: Buscar carta
 *    description: Busca una carta en especifico
 *    parameters:
 *    - name: id
 *      description: Id de la carta
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *            200:
 *                description: Pedido exitoso
 *            404:
 *                description: Usuario o producto no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */


 router.get("/card/:id", (req, res) => {
   middle.showACard(req,res);

})



/**
 * @swagger
 * /newCard:
 *  post:
 *    tags: 
 *      [Cards]
 *    summary: Crear carta
 *    description: Crea una nueva carta
 *    parameters:
 *    - name: name
 *      description: Nombre de la carta (Ingles)
 *      in: formData
 *      required: true
 *      type: string
 *    - name: nameJP
 *      description: Nombre de la carta (Japones)
 *      in: formData
 *      required: true
 *      type: string
 *    - name: starts
 *      description: Cantidad de estrellas
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: atk
 *      description: Puntos de ataque
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: def
 *      description: Puntos de defensa
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: ydb
 *      description: ID Konami
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: type
 *      description: Raza de la carta
 *      in: formData
 *      required: true
 *      type: string
 *    - name: tCard
 *      description: Tipo de carta
 *      in: formData
 *      required: true
 *      type: string
 *    - name: attribute
 *      description: Atributo de la carta
 *      in: formData
 *      required: true
 *      type: string
 *    - name: urlImg
 *      description: Url donde se guarda la img de la carta
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *            200:
 *                description: Pedido exitoso
 *            404:
 *                description: Usuario o producto no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */

router.post("/newCard", (req, res) => {

    middle.createAcard(req, res);

});


/**
 * @swagger
 * /addDescription/{id}:
 *  put:
 *    tags: 
 *      [Cards]
 *    summary: Agregar descripcion
 *    description: Agrega una descripcion a la carta
 *    parameters:
 *    - name: id
 *      description: Id de la carta
 *      in: path
 *      required: true
 *      type: integer
 *    - name: description
 *      description: Descripcion de la carta
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *            200:
 *                description: Pedido exitoso
 *            404:
 *                description: Usuario o producto no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */

router.put("/addDescription/:id", (req, res) => {
    middle.addDescritionAcard(req, res);
});

/**
 * @swagger
 * /deleteCard/{id}:
 *  delete:
 *    tags: 
 *      [Cards]
 *    summary: Eliminar carta
 *    description: Elimina una carta
 *    parameters:
 *    - name: id
 *      description: Id de la carta
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *            200:
 *                description: Pedido exitoso
 *            404:
 *                description: Usuario o producto no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */

 router.delete("/deleteCard/:id", (req, res) => {
    middle.deleteACard(req, res);
});

/**
 * @swagger
 * /modCard/{id}:
 *  put:
 *    tags: 
 *      [Cards]
 *    summary: Modificar carta
 *    description: Modifica uno o mas parametros de una carta
 *    parameters:
 *    - name: id
 *      description: Id de la carta
 *      in: path
 *      required: true
 *      type: integer
 *    - name: name
 *      description: Nombre de la carta (Ingles)
 *      in: formData
 *      required: false
 *      type: string
 *    - name: nameJP
 *      description: Nombre de la carta (Japones)
 *      in: formData
 *      required: false
 *      type: string
 *    - name: starts
 *      description: Cantidad de estrellas
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: atk
 *      description: Puntos de ataque
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: def
 *      description: Puntos de defensa
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: type
 *      description: Raza de la carta
 *      in: formData
 *      required: false
 *      type: string
 *    - name: tCard
 *      description: Tipo de carta
 *      in: formData
 *      required: false
 *      type: string
 *    - name: attribute
 *      description: Atributo de la carta
 *      in: formData
 *      required: false
 *      type: string
 *    - name: urlImg
 *      description: Url donde se guarda la img de la carta
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Pedido exitoso
 *            404:
 *                description: Usuario o producto no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */

 router.put("/modCard/:id", (req, res) => {

  middle.modifyACard(req,res);
});

module.exports = router;