require('dotenv').config();
const mongoose = require('mongoose');

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}

const schemaDescription = {
    description : String
}

const schemaCard  ={
    name : String,
    nameJP : String,
    starts : Number,
    atk:Number,
    def:Number,
    YDB:Number,
    type:String,
    tCard:String,
    attribute:String,
    urlImg : String,
    description : []
}

const modelCard = mongoose.model("cards",schemaCard);


const MONGODB_HOST = process.env.MONGODB_HOST;
const MONGODB_PORT = process.env.MONGODB_PORT;
const MONGODB_MOVIE_DB_NAME = process.env.MONGODB_MOVIE_DB_NAME;

const conectionString = `mongodb://${MONGODB_HOST}:${MONGODB_PORT }/${MONGODB_MOVIE_DB_NAME}`;

mongoose.connect(conectionString,options);

module.exports = {
    mongoose,
    modelCard,
    schemaDescription
};